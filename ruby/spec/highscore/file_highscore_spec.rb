require 'spec_helper'

describe Highscore::FileHighscore do
  before do
    @highscore = Highscore::FileHighscore.new 'temp_scores.txt'
  end
  
  after do
    File.delete 'temp_scores.txt'
  end

  it 'adds score' do
    expect(@highscore.get_scores.empty?).to eq true
    @highscore.add_score('Rocky', 12)
    expect(@highscore.get_scores.size).to eq 1
    @highscore.add_score('Rubble', 10)
    expect(@highscore.get_scores.size).to eq 2
    @highscore.add_score('Rubble', 12)
    expect(@highscore.get_scores.size).to eq 2
    @highscore.add_score('Rubble', 8)
    expect(@highscore.get_scores.size).to eq 2
  end

  it 'gets scores' do
    @highscore.add_score('Rocky', 12)
    @highscore.add_score('Rubble', 8)
    @highscore.add_score('Chase', 10)

    expect(@highscore.get_scores.size).to eq 3
  end

  it 'clear scores' do
    @highscore.add_score('Rocky', 14)
    @highscore.clear_scores

    expect(@highscore.get_scores.empty?).to eq true
  end
end