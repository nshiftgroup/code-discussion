class Game
  def initialize args = {}
    @console_ui = UI::ConsoleUI.new
    @highscore = Highscore::FileHighscore.new

    @guesses = []
  end

  def start
    @console_ui.show_instructions

    player = @console_ui.ask_for_player_name

    if player == 'admin'
      @highscore.clear_scores if @console_ui.should_clear_scores?
      return
    end

    while true
      start_round

      @highscore.add_score(player, @guesses.size)
      @console_ui.show_guesses(@guesses)
      @console_ui.show_highscores(@highscore.get_scores)

      break @console_ui.say_goodbye unless @console_ui.ask_for_another_game
    end
  end

  def start_round
    @guesses = []

    number = rand(1000)

    guess = @console_ui.ask_for_number_from_player
    @guesses << guess

    while guess != number
      guess < number ? @console_ui.prompt_higher : @console_ui.prompt_lower

      guess = @console_ui.ask_for_number_from_player
      @guesses << guess
    end

    @console_ui.correct
  end
end