module UI
  class ConsoleUI
    def initialize args = {}
    end

    def ask_for_player_name
      puts "Enter player name: "
      player = gets.chomp
      puts "Welcome #{player}"

      player
    end

    def show_instructions
      puts "--- Welcome to Guessing Game ---"
      puts "I am thinking of a number between 0 and 1000, your job is to guess the number!"
    end

    def ask_for_number_from_player
      puts "What is your guess?"

      gets&.chomp.to_i
    end

    def prompt_higher
      puts "Incorrect, my number is higher."
    end

    def prompt_lower
      puts "Incorrect, my number is lower."
    end

    def correct
      puts "Congratulations, you guessed it!"
    end

    def show_highscores scores
      scores.sort!{|a, b| a.num_guesses <=> b.num_guesses}
      puts "--- Highscore ---"
      puts "#{'Player'.ljust(10)} Guesses"

      scores.each do |score|
        puts "#{score.player.ljust(10)} #{score.num_guesses}"
      end
    end

    def ask_for_another_game
      puts "Play again? (Y/N): "
      
      gets&.chomp == 'Y'
    end

    def show_guesses guesses
      puts "Num guesses = #{guesses.size}"
      puts "Your guesses = #{guesses}"
    end

    def say_goodbye
      puts "Thanks for playing!"
    end 

    def should_clear_scores?
      puts "Clear scores? (Y/N)"
      
      gets.chomp == 'Y'
    end
  end
end