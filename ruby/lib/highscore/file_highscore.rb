module Highscore
  class FileHighscore
    def initialize file_path = nil
      file_path = 'lib/highscore/file/highscore.txt' if file_path.nil? || file_path.empty?
      @file_path = file_path
    end

    def add_score player, num_guesses
      scores = load_scores
      existing_score = get_score_for_player(player)

      if existing_score.nil?
        scores << Score.new(player, num_guesses)
        store_scores(scores)
      elsif num_guesses < existing_score.num_guesses
        scores.reject!{|score| score.player == player}
        scores << Score.new(player, num_guesses)
        store_scores(scores)
      end
    end

    def get_scores
      load_scores
    end

    def clear_scores
      clear
    end

    private

    def get_score_for_player player
      get_scores.to_a.find{|score| score.player == player}
    end

    def load_scores
      scores = []

      open_file 'r' do |file|
        file.readlines.each do |line|
          scores << parse_line(line.chomp)
        end
      end

      scores
    end

    def store_scores scores
      open_file 'w' do |file|
        file.write scores.map{|score| "#{score.player}:#{score.num_guesses}"}.join("\n")
      end
    end

    def parse_line line
      name_and_guesses = line.to_s.split(':')
      Score.new(name_and_guesses[0], name_and_guesses[1].to_i)
    end

    def clear
      # Opening file in write mode, creates empty file
      open_file 'w' do |file|
      end
    end
    
    def open_file mode = 'r'
      File.open(@file_path, 'w') {} unless File.exist?(@file_path)

      File.open(@file_path, mode) do |file|
        yield(file)
      end
    end
    
  end
end