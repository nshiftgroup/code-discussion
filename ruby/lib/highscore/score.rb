module Highscore
  class Score
    def initialize player, num_guesses
      @player = player
      @num_guesses = num_guesses
    end

    def player= player
      @player = player
    end

    def player
      @player
    end

    def num_guesses= num_guesses
      @num_guesses = num_guesses
    end

    def num_guesses
      @num_guesses
    end

    def to_s
      "Score{player='#{player}', num_guesses=#{num_guesses}}"
    end
  end
end