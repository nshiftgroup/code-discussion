package guessinggame;

import guessinggame.highscore.FileHighscore;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FileHighscoreTest {

    private FileHighscore highscore;
    private final File highScoreFile = new File("tempScores.txt");

    @Before
    public void setUp() throws IOException {
        highscore = new FileHighscore(highScoreFile.getName());
        assertTrue(highScoreFile.createNewFile());
    }

    @After
    public void tearDown() {
        assertTrue(highScoreFile.delete());
    }

    @Test
    public void addScore() throws IOException {
        assertTrue(highscore.getScores().isEmpty());
        highscore.addScore("Rocky", 12);
        assertEquals(1, highscore.getScores().size());
        highscore.addScore("Rubble", 10);
        assertEquals(2, highscore.getScores().size());
        highscore.addScore("Rubble", 12);
        assertEquals(2, highscore.getScores().size());
        highscore.addScore("Rubble", 8);
        assertEquals(2, highscore.getScores().size());
    }

    @Test
    public void getScores() throws IOException {
        highscore.addScore("Rocky", 12);
        highscore.addScore("Rubble", 8);
        highscore.addScore("Chase", 10);

        assertEquals(3, highscore.getScores().size());
    }

    @Test
    public void clearScores() throws IOException {
        highscore.addScore("Rocky", 14);
        highscore.clearScores();

        assertTrue(highscore.getScores().isEmpty());
    }
}