package guessinggame;

import guessinggame.highscore.FileHighscore;
import guessinggame.ui.ConsoleUI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class Game {

    private ConsoleUI consoleUI;
    private FileHighscore highscore;
    private int number;
    private List<Integer> guesses;

    Game() {
        consoleUI = new ConsoleUI();
        highscore = new FileHighscore();
    }

    void start() throws IOException {
        consoleUI.showInstructions();

        String player = consoleUI.askForPlayerName();
        if (player.equals("admin")) {
            if (consoleUI.shouldClearScores()) {
                highscore.clearScores();
            }
            return;
        }

        while (true) {
            startRound();
            highscore.addScore(player, guesses.size());
            consoleUI.showGuesses(guesses);
            consoleUI.showHighscores(highscore.getScores());
            boolean playAgain = consoleUI.askForAnotherGame();
            if (!playAgain) {
                consoleUI.sayGoodbye();
                break;
            }
        }
    }

    private void thinkOfNumber() {
        Random rand = new Random();
        number = rand.nextInt(1000);
    }

    private void startRound() {
        guesses = new ArrayList<>();

        thinkOfNumber();

        int guess = consoleUI.askForNumberFromPlayer();
        guesses.add(guess);
        while (guess != number) {
            if (guess < number) {
                consoleUI.promptHigher();
            } else {
                consoleUI.promptLower();
            }
            guess = consoleUI.askForNumberFromPlayer();
            guesses.add(guess);
        }
        consoleUI.correct();
    }
}
