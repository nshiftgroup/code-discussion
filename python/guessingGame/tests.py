import os
import unittest
from unittest.mock import patch

from guessingGame.highscore import FileHighscore


class TestGuessingGame(unittest.TestCase):

    def setUp(self):
        self.high_score_file = 'tempScores.txt'
        self.highscore = FileHighscore(self.high_score_file)
        self.highscore.create_file()

    def tearDown(self):
        if os.path.exists(self.high_score_file):
            os.remove(self.high_score_file)

    def test_add_score(self):
        self.assertTrue(self.highscore.get_scores() == [])
        self.highscore.add_score("Rocky", 12)
        self.assertEqual(len(self.highscore.get_scores()), 1)

        self.highscore.add_score("Rubble", 10)
        self.assertEqual(len(self.highscore.get_scores()), 2)

        self.highscore.add_score("Rubble", 12)  # Updating score
        self.assertEqual(len(self.highscore.get_scores()), 2)  # Size remains 2 as Rubble's score is updated

        self.highscore.add_score("Rubble", 8)  # Rubble again with a new score
        self.assertEqual(len(self.highscore.get_scores()), 2)  # Size remains 2

    def test_get_scores(self):
        self.highscore.add_score("Rocky", 12)
        self.highscore.add_score("Rubble", 8)
        self.highscore.add_score("Chase", 10)

        self.assertEqual(len(self.highscore.get_scores()), 3)

    def test_clear_scores(self):
        self.highscore.add_score("Rocky", 14)
        self.highscore.clear_scores()
        self.assertTrue(self.highscore.get_scores() == [])
