class Score:
    def __init__(self, player, num_guesses):
        self.player = player
        self.num_guesses = num_guesses

    def set_player(self, player):
        self.player = player

    def set_num_guesses(self, num_guesses):
        self.num_guesses = num_guesses

    def get_player(self):
        return self.player

    def get_num_guesses(self):
        return self.num_guesses

    def __str__(self):
        return f"Score{{player='{self.player}', num_guesses={self.num_guesses}}}"
