import os

from guessingGame.highscore import Score


class FileHighscore:

    def __init__(self, file_name="highscore.txt"):
        self.file_name = os.path.join(os.path.dirname(__file__), "file", file_name)

    def load_scores(self):
        scores = []
        if not os.path.exists(self.file_name):
            raise FileNotFoundError(f"File {self.file_name} not found")
        with open(self.file_name, "r") as file:
            for line in file:
                if line.strip() != "":
                    scores.append(self.parse_lines(line))
        return scores

    def parse_lines(self, line:str):
        player, numGuesses = line.strip().split(":")
        return Score(player, int(numGuesses))

    def get_scores(self):
        return self.load_scores()

    def clear_scores(self):
        if not os.path.exists(self.file_name):
            raise FileNotFoundError("No highscore file")
        with open(self.file_name, "w") as file:
            file.write("")


    def add_score(self, player:str, num_guesses:int):
        scores = self.load_scores()
        existing_score = self.get_score_for_player(player)
        if not existing_score:
            scores.append(Score(player, num_guesses))
            self.store(scores)
        elif num_guesses < existing_score.get_num_guesses():
            scores = [score for score in scores if score.get_player() != player]
            scores.append(Score(player, num_guesses))
            self.store(scores)


    def get_score_for_player(self, player:str) -> Score:
        scores = self.get_scores()
        for score in scores:
            if score.get_player() == player:
                return score
        return None

    def store(self, scores: list):
        if not os.path.exists(self.file_name):
            raise FileNotFoundError(f"No highscore file")
        with open(self.file_name, "w") as file:
            for score in scores:
                file.write(f"{score.get_player()}:{score.get_num_guesses()}\n")

    def create_file(self):
        if not os.path.exists(self.file_name):
            with open(self.file_name, "w") as file:
                file.write("")
