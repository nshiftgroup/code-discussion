import random
from guessingGame.ConsoleUI import ConsoleUI
from guessingGame.highscore import FileHighscore


class Game:

    def __init__(self):
        self.console_UI = ConsoleUI()
        self.highscore = FileHighscore()
        self.guesses = []
        self.number = None

    def start(self):
        self.console_UI.show_instructions()
        player_name = self.console_UI.ask_player_name()
        if player_name == "admin":
            if self.console_UI.clear_scores():
                self.highscore.clear_scores()
        while True:
            self.start_round()
            self.highscore.add_score(player_name, len(self.guesses))
            self.console_UI.show_guesses(self.guesses)
            self.console_UI.show_highscores(self.highscore.get_scores())
            if not self.console_UI.ask_for_another_game():
                break
            self.guesses = []

    def think_of_number(self):
        self.number = random.randint(0, 1000)

    def start_round(self):
        self.think_of_number()
        guessed_nr = self.console_UI.ask_number_from_player()
        self.guesses.append(guessed_nr)
        while guessed_nr != self.number:
            if guessed_nr < self.number:
                self.console_UI.prompt_higher_guess()
            else:
                self.console_UI.prompt_lower_guess()
            guessed_nr = self.console_UI.ask_number_from_player()
            self.guesses.append(guessed_nr)
        self.console_UI.show_correct_guess()

