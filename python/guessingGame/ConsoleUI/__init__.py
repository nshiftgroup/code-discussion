from typing import List
from guessingGame.highscore import Score


class ConsoleUI:

    def ask_player_name(self) -> str:
        return input("Enter your name: ")

    def show_instructions(self):
        print("\n--- Welcome to Guessing Game ---")
        print("\nI am thinking of a number between 0 and 1000, your job is to guess the number!")

    def ask_number_from_player(self) -> int:
        return int(input("\nEnter your guess: "))

    def prompt_higher_guess(self):
        print("Incorrect, my number is higher. Try again!")

    def prompt_lower_guess(self):
        print("Incorrect, my number is lower. Try again!")

    def show_correct_guess(self):
        print(f"Congratulations! You guessed the number")

    def show_highscores(self, scores: List[Score]):
        print(f"\n{'--- Highscore ---':>25}")
        print(f"{'Pos':<4} {'Player':>15} {'Guesses':>15}")
        scores.sort(key=lambda score: score.num_guesses)

        result = []
        for i, score in enumerate(scores):
            result.append(f"{i + 1:<4} {score.player:>15} {score.num_guesses:>15}")
        print("\n".join(result))

    def ask_for_another_game(self) -> bool:
        return input("Do you want to play again? (y/n): ").lower() == 'y'

    def show_goodbye(self):
        print("\nThank you for playing the game. Goodbye!")

    def show_guesses(self, guesses: list):
        print(f" Num guesses = {len(guesses)}")
        print(f"Your guesses = {guesses}")

    def clear_scores(self) -> bool:
        return input("Do you want to clear the high scores? (y/n): ").lower() == 'y'