﻿namespace GuessingGame.HighScore
{
    public class Score
    {
        private string player;
        private int numGuesses;

        internal Score(string player, int numGuesses)
        {
            this.player = player;
            this.numGuesses = numGuesses;
        }

        public string Player
        {
            set
            {
                this.player = value;
            }
            get
            {
                return player;
            }
        }

        public int NumGuesses
        {
            set
            {
                this.numGuesses = value;
            }
            get
            {
                return numGuesses;
            }
        }

        public override string ToString()
        {
            return "Score{" + "player='" + player + '\'' + ", numGuesses=" + numGuesses + '}';
        }
    }
}