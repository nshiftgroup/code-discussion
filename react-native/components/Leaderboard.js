import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default function Leaderboard({ scores }) {
    return (
        <View style={styles.leaderboard}>
            <Text style={styles.title}>Leaderboard:</Text>
            <View>
            { scores.sort((a, b) => a.score - b.score).map(({ name, score }) => (
                    <Text key={name + '-' + score} style={styles.score}>{name}: {score}</Text>
            )) }
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    leaderboard: {
        marginVertical: 32,
    },
    title: {
        fontSize: 18,
    },
    score: {
        marginVertical: 3,
    }
})
