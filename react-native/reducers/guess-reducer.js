import {isSetGuessAction} from '../actions/guess-actions';

export default function guessReducer(state= '', action) {
    if (isSetGuessAction(action)) {
        return action.guess;
    }

    return state;
}
