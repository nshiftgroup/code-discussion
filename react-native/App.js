import React from 'react';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import GuessingGame from './components/GuessingGame';
import guessReducer from './reducers/guess-reducer';

const App = () => {

    const store = Redux.createStore(
        Redux.combineReducers({
            guess: guessReducer,
        }),
    );

    const mapStateToProps = (state) => {
        return {
            guess: state.guess,
        };
    };

    const mapDispatchToProps = {
        setGuess: (guess = '') => ({type: 'SET_GUESS', guess: guess}),
    };

    const ConnectedGuessingGame = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(GuessingGame);

    return <ReactRedux.Provider store={store}>
        <ConnectedGuessingGame difficulty={100}/>
    </ReactRedux.Provider>;
};

export default App;
