const SET_GUESS = 'SET_GUESS';

export function isSetGuessAction(action) {
    return action.type === SET_GUESS;
}
