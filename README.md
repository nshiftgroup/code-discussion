# Welcome to the code discussion!
This is a simple guessing game where the objective is to find out what number the computer is thinking about. This little challenge focuses on starting discussions around code quality, testability, refactoring etc. rather than being a complex programming test.

## Objective
1. Clone the repository and select a project relevant to the position you are applying for.
2. Run the code and try out the application.
3. Look through the code, look for improvements, take notes.
4. During a video call, walk us through the code and suggest improvements.

__Suggested areas of discussion__

- How would you implement a multiplayer feature where two players takes turn guessing the correct number?
- If two players use exactly 3 guesses each to find the correct number their score will be the same. How can we implement another factor for scoring the players?
- Would you refactor or rewrite the game if you get new requirements that for example the game should be about guessing cities or colors?
- How would you implement the game in another programming language? What language and why?
- How would you improve the UI?
- What do you think about the test coverage?
- How can you make the game harder or easier to win?

## Instructions for Python
__Requirements__

A working installation of [Python](https://www.python.org/) with [pip](https://pypi.org/project/pip/).

__Setup__
1. Clone the repo to your local drive (the folder you will be working with is `python`).
2. Import the project into your favorite editor.
3. Set up the python interpreter to use the correct version of Python.
4. Install the required packages by running `pip install -r .\python\requirements.txt`.
5. Run the code by executing `python .\python\main.py`.

## Instructions for Java

__Requirements__

A working installation of [Java JDK](https://openjdk.java.net/) with [Gradle](https://gradle.org/).

__Setup__

1. Clone the repo to your local drive (the folder you will be working with is `java`).
2. Import as Gradle project using [IntelliJ](https://www.jetbrains.com/idea/) or [Eclipse](https://www.eclipse.org/). Or just use your favorite editor.
3. For IntelliJ: Install the Plant-UML and Graphviz plugin to view the UML diagram. Intellij will prompt you to install the plugin. Use a package manager like [APT](https://en.wikipedia.org/wiki/APT_%28software%29) or [DNF](https://en.wikipedia.org/wiki/DNF_%28software%29) to install Graphviz.
4. For Eclipse: Install Eclipse buildship for Gradle support.

__The code__

The program entry point is `guessinggame.Main` in `java/src/main/java/guessinggame/`

__Run the code__

1. Run Guessing game from gradle task: `application -> run`.
2. Run tests from gradle task: `verification -> test`.

## Instructions for TypeScript + React

__Requirements__

A working installation of [Node.js](https://nodejs.org/) with npm.

__Setup__

1. Clone the repo to your local drive (the folder you will be working with is `ts-react`).

__The code__

The program entry point is `main.tsx` in `ts-react/src/`

__Run the code__

1. `npm install`.
2. `npm run build`.
3. Open `index.html` in your browser.

## Instructions for React Native

__Requirements__

[Environment setup](https://reactnative.dev/docs/environment-setup)

__Setup__

1. Clone the repo to your local drive (the folder you will be working with is `react-native`).

__The code__

The program entry point is `App.js`

__Run the code__

1. `npm install` (`cd ios && pod install` for iOS).
2. `npm run android` or `npm run ios`.

## Instructions for TypeScript + Vue

__Requirements__

A working installation of [Node.js](https://nodejs.org/) version >= 12.0.0 with npm.

__Setup__

1. Clone the repo to your local drive (the folder you will be working with is `vue`).

__The code__

The program entry point is `main.ts` in `vue/src/`

__Run the code__

1. `npm install`.
2. `npm run dev`.
3. Browse to the URL provided by the npm script. (e.g. [http://localhost:3000/](http://localhost:3000/))

## Instructions for C\#

__Requirements__

Have visual studio with .NET 5 SDK.

__Setup__

1. Clone the repo to your local drive (the folder you will be working with is .`net`).

__The code__

The main entry point is `Program` in `.net/src/main/Programs.cs`

__Run the code__

1. For visual studio: Run Guessing game if you have selected as startup project GuessingGame project.
2. Run tests from FileHighScoreTest.cs from the the project test.

## Instructions for Ruby

**Requirements**

A working installation of Ruby (~2.5)

**Setup**

1. Clone the repo to your local drive (the folder you will be working with is `ruby`).

**The code**

The main entry point is `main.rb` in `ruby/main.rb`

**Run the code**

1. `ruby main.rb`

## Maintainers
max.almgren@nshift.com
