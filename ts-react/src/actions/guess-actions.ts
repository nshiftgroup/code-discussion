import Redux from 'redux';

const SET_GUESS = 'SET_GUESS';

export interface SetGuessAction extends Redux.Action<typeof SET_GUESS> {
    guess: string;
}

export function isSetGuessAction(action: Redux.Action<any>): action is SetGuessAction {
    return action.type === SET_GUESS;
}
